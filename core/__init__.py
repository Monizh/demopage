import os
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template, request
import psycopg2
import psycopg2.extras
import config

app = Flask(__name__)
app.config.from_object('config')

myConnection = psycopg2.connect( host="localhost", user="postgres", password="password", dbname="buyer")
cur = myConnection.cursor()

db = SQLAlchemy(app)
db.init_app(app)

from core .auth import bp_root
from core.bills import bp_bills
from core.buyers import bp_buy

from core.buyers import models  
from core.buyers import views
# from core.buyers import *

from core.auth import views 

app.register_blueprint(bp_root, url_prefix='/')
app.register_blueprint(bp_bills, url_prefix='/bills')
app.register_blueprint(bp_buy, url_prefix='/')


# @app.route('/<name>')
# def hello_name(name):
#     return "Hello {}!".format(name)


@app.before_first_request
def create_tables():
    db.create_all() 

# @app.cli.command()
# def create_table():
#     db.create_all()






