import os
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from core import db, cur, app

from . import bp_bills


@bp_bills.route('/purchase', methods=['GET'])
def purchase_get():
    return render_template('client/purchase.html')