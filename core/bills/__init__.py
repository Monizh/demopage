from flask import Blueprint
from core import app

bp_bills = Blueprint('bp_bills', __name__)


from . views import *
from . models import *