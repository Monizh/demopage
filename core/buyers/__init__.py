from flask import Flask, Blueprint, request, render_template
from core import app

bp_buy = Blueprint('bp_buy', __name__)

from . import models
from . import views
from . import webpush_handler