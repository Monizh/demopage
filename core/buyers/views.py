import os
import config
from flask import Flask, Blueprint, render_template, request, redirect, json, jsonify, session, g, make_response
from flask_sqlalchemy import SQLAlchemy
from core.buyers.webpush_handler import trigger_push_notifications_for_subscriptions

from core import db
from core.buyers.models import BuyersModel
from core.buyers import bp_buy
import stripe

from config import stripe_keys

products = {
    'megatutorial': {
        'name': 'The Flask Mega-Tutorial',
        'price': 3900,
    },
    'support': {
        'name': 'Python 1:1 support',
        'price': 20000,
        'per': 'hour',
    },
}

@bp_buy.route("/", methods=['GET'])
def index():
    return render_template('content/index.html')

@bp_buy.route("/order", methods=['GET'])
def order_checkout():
    return render_template('content/order.html', products=products)

@bp_buy.route('/order/success')
def success():
    return render_template('content/success.html')


@bp_buy.route('/order/cancel')
def cancel():
    return render_template('content/cancel.html')

@bp_buy.route('order/<product_id>', methods=['POST'])
def order(product_id):
    if product_id not in products:
        abort(404)

    checkout_session = stripe.checkout.Session.create(
        line_items=[
            {
                'price_data': {
                    'product_data': {
                        'name': products[product_id]['name'],
                    },
                    'unit_amount': products[product_id]['price'],
                    'currency': 'usd',
                },
                'quantity': 1,
            },
        ],
        payment_method_types=['card'],
        mode='payment',
        success_url=request.host_url + 'order/success',
        cancel_url=request.host_url + 'order/cancel',
    )
    return redirect(checkout_session.url)

@bp_buy.route("/python", methods=['GET'])
def python():
    return render_template('content/python.html')

@bp_buy.route("/webapp", methods=['GET'])
def webapp():
    return render_template('content/webapp.html', products=products)

@bp_buy.route("/checkout", methods=['GET'])
def checkout():
    return render_template('content/checkout.html', key=stripe_keys['publishable_key'])

@bp_buy.route("/charge", methods=["POST"])
def charge():
    # Amount in rupees
    amount = 500

    customer = stripe.Customer.create(
        email='customer@example.com',
        source=request.form['stripeToken']
    )

    charge = stripe.Charge.create(
        customer=customer.id,
        amount=amount,
        currency='usd',
        description='Flask Charge'
    )

    return render_template('charge.html', amount=amount)

@bp_buy.route("/contact", methods=['GET'])
def contact_get():
    return render_template('content/contact.html')

@bp_buy.route('/contact', methods=['POST'])
def contact_post():
    data = {
        # 'id': 1,
        'fullname': request.form['fullname'],
        'email': request.form['email'],
        'phoneno': request.form['phoneno'],
        'message': request.form['message'],
    }
    # print(data)
    buyersdt = BuyersModel(**data)
    # print(buyersdt)
    db.session.add(buyersdt)
    db.session.commit()

    return redirect(request.url)
    

@bp_buy.route("/about", methods=['GET'])
def about():
    return render_template('content/about.html')


@bp_buy.route("/admin", methods=['GET'])
def admin_get():
    return render_template("admin.html")


@bp_buy.route("/admin-api/trigger-push-notifications", methods=["POST"])
def trigger_push_notifications():
    print("hello")
    json_data = request.get_json()
    subscriptions = PushSubscription.query.all()
    results = trigger_push_notifications_for_subscriptions(
        subscriptions, 
        json_data.get('title'),
        json_data.get('body')
    )
    
    return jsonify({
        "status": "success",
        "result": results
    })

@bp_buy.route("/api/push-subscriptions", methods=["POST"])
def create_push_subscription():
    json_data = request.get_json()
    subscription = PushSubscription.query.filter_by(
        subscription_json=json_data['subscription_json']
    ).first()
    if subscription is None:
        subscription = PushSubscription(
            subscription_json=json_data['subscription_json']
        )
        db.session.add(subscription)
        db.session.commit()
        return jsonify({
            "status": "success"
        })