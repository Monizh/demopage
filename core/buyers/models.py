from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from core import db

class BuyersModel(db.Model):
    __tablename__ = 'buyers'

    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(45), nullable=False )
    email = db.Column(db.String(100))
    phoneno = db.Column(db.String(10))
    message = db.Column(db.String(50))


    def __init__(self,  fullname, email, phoneno, message):
        # self.id = id
        self.fullname = fullname
        self.email = email
        self.phoneno = phoneno
        self.message = message


    # def create_record(self):
    #     db.session.add(self)
    #     db.session.commit()

class PushSubscription(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    subscription_json = db.Column(db.Text, nullable=False)
