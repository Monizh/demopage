from flask import Blueprint
from core import app

bp_root = Blueprint('bp_root', __name__)

from . views import *
from . models import *