
from core import db, app, cur
from flask import Flask, render_template, request, redirect, url_for, flash, jsonify, session, g, make_response

from . import bp_root

@app.route('/login', methods=['GET'])
def mainlayout():
    return render_template('client/login.html')


@bp_root.route('/home', methods=['GET'])
def mainlayout():
    return render_template('client/home.html')